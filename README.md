# English Questions
 
## About this project
- This project ask you questions in english.
- This project can be helpful for anyone looking for practice english (alone or not) and need something to start the conversation.

## Learning project
This project is for me an exercise to practice frameworks and technologies : 
- React
- NodeJS
- MongoDB
- All of this using Typescript

## Installation

```bash
cp docker-compose.example.yml docker-compose.yml
```

```bash
cp api/.env.example api/.env
```

```bash
cp web/.env.example web/.env
```

```bash
docker-compose up -d --build
```

```bash
Go to http://localhost:3000
```

## TODO

- Create Jobs in GitlabCI for coding style (prettier & eslint)
- Deploy the app in GCP with Terraform and GitlabCI
